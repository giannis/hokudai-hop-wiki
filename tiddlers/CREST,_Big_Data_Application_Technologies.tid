created: 20160729075228698
creator: j
modified: 20210222025309150
modifier: truemrwalker
tags: $:/tags/Stylesheet TableOfContents
title: CREST, Big Data Application Technologies
type: text/vnd.tiddlywiki

<style>

.cosmos{background-image: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1)), url(<$macrocall $name="datauri" title="DarkMatter2.jpg" $output="text/plain"/>);}

.agriculture{background-image: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1)), url(<$macrocall $name="datauri" title="OpenField.jpg" $output="text/plain"/>);}

.animaldev{background-image: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1)), url(<$macrocall $name="datauri" title="Animal-mRna.png" $output="text/plain"/>);}

.tsunami{background-image: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1)), url(<$macrocall $name="datauri" title="tsunami.jpg" $output="text/plain"/>);}

.virus{background-image: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1)), url(<$macrocall $name="datauri" title="virus.jpg" $output="text/plain"/>);}

.drugs{background-image: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1)), url(<$macrocall $name="datauri" title="drugs.jpg" $output="text/plain"/>);}

.knowledge{background-image: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1)), url(<$macrocall $name="datauri" title="knowledge.jpg" $output="text/plain"/>);}

.pandemic{background-image: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1)), url(<$macrocall $name="datauri" title="SARS-CoV-2.jpg" $output="text/plain"/>);}

.storm{background-image: linear-gradient(to right, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.1)), url(<$macrocall $name="datauri" title="storm2.jpg" $output="text/plain"/>);}

</style>
<span style="font-size: 30px; font-weight: bold; color: #e8e22a;">Advanced Application Technologies to Boost Big Data Utilization for Multiple-Field Scientific Discovery and Social Problem Solving.</span>

This is the Hands-on Portal for the [[Japan Science and Technology Agency (JST)|http://www.jst.go.jp/EN/index.html]] [[CREST|http://www.jst.go.jp/kisoken/crest/en/]] funding program for [["Advanced Application Technologies to Boost Big Data Utilization for Multiple-Field Scientific Discovery and Social Problem Solving"|http://www.jst.go.jp/kisoken/crest/en/research_area/ongoing/areah25-5.html]]. 

The Hands-on Portal is a site where you can find information on the research done under this funding program and where there are also applications that you can try yourself. There are example applications and in-depth information and media, as well as background information and helpful explanations, that not only shows the results of these research projects, but also teach about the research area in general, how the research is done or why it is useful.

There are 9 different research projects available within the CREST-Hands-on-Portal that you can examine and learn more about, all accessible from below or from the right-side navigation panel.

! <span style="text-decoration-line: underline; text-decoration-style: double;">Projects within the funding program</span>

<div class="agriculture" style="background-size: 100% 100%; padding: 50px;">

* <$link to="Agriculture Data - Hirafuji Group"><span style="font-size: 25px; color: #f2ff69; font-weight: bold;">Agriculture Data - Hirafuji Group</span></$link>
** <span style="font-size: 18px; color: orchid; font-weight: bold">Research Director:</span> <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Masayuki Hirafuji"/><span style="font-size: 22px; color: lightblue; font-weight:bold;">//Masayuki Hirafuji//</span></$button> (Professor of Agriculture, Tokyo Univ. / Director, Hokkaido Agricultural Research Center, National Agriculture and Food Research Organization).
** <span style="font-size: 18px; color: orchid; font-weight: bold">Complete Name:</span> <span style="font-size: 17px; font-style: italic; font-weight: bold">//"Knowledge Discovery by Constructing ~AgriBigData"//</span>
** [[Project information at CREST site|http://www.jst.go.jp/kisoken/crest/en/project/44/15656456.html]].
** [[Hirafuji Project information at CREST Symposium Site|http://dataia.eu/sites/default/files/DATAIA_JST_International_Symposium/15_CREST-Paris201807Hirafuji.pdf]]

</div>

<br>

<div class="animaldev" style="background-size: 100% 100%; padding: 50px;">

* <$link to="Animal Development - Onami Group"><span style="font-size: 25px; color: #f2ff69; font-weight: bold;">Animal Development - Onami Group</span></$link>
** <span style="font-size: 18px; color: orchid; font-weight: bold">Research Director:</span> <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Shuichi Onami"/><span style="font-size: 22px; color: lightblue; font-weight:bold;">//Shuichi Onami//</span></$button> (Team Leader, Quantitative Biology Center, RIKEN).
** <span style="font-size: 18px; color: orchid; font-weight: bold">Complete Name:</span> <span style="font-size: 17px; font-style: italic; font-weight: bold">//"Data-Driven Analysis of the Mechanism of Animal Development"//</span>
** [[Project information at CREST site|http://www.jst.go.jp/kisoken/crest/en/project/44/15656709.html]].
** [[Onami Group information at Research Laboratory site|https://so.riken.jp]]

</div>

<br>

<div class="cosmos" style="background-size: 100% 100%; padding: 50px;">

* <$link to="Cosmology - Yoshida Group"><span style="font-size: 25px; color: #f2ff69; font-weight: bold;">Cosmology - Yoshida Group</span></$link>
** <span style="font-size: 18px; color: orchid; font-weight: bold">Research Director:</span> <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Naoki Yoshida"/><span style="font-size: 22px; color: lightblue; font-weight:bold;">//Naoki Yoshida//</span></$button> (Professor, Department of Physics, The University of Tokyo / Kavli IPMU).
** <span style="font-size: 18px; color: orchid; font-weight: bold">Complete Name:</span> <span style="font-size: 17px; font-style: italic; font-weight: bold">//"Statistical Computational Cosmology with Big Astronomical Imaging Data"//</span>
** [[Project information at CREST site|http://www.jst.go.jp/kisoken/crest/en/project/44/14532369.html]].
** [[KAVLI IPMU Research site|https://www.ipmu.jp/en]].
</div>

<br>

<div class="tsunami" style="background-size: 100% 100%; padding: 50px;">

* <$link to="Disaster Management - Koshimura Group"><span style="font-size: 25px; color: #f2ff69; font-weight: bold;">Disaster Management - Koshimura Group</span></$link>
** <span style="font-size: 18px; color: orchid; font-weight: bold">Research Director:</span> <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Shunichi Koshimura"/><span style="font-size: 22px; color: lightblue; font-weight:bold;">//Shunichi Koshimura//</span></$button> (Professor, International Research Institute of Disaster Science, Tohoku University).
** <span style="font-size: 18px; color: orchid; font-weight: bold">Complete Name:</span> <span style="font-size: 17px; font-style: italic; font-weight: bold">//"Establishing the Most Advanced Disaster Reduction Management System by Fusion of Real-Time Disaster Simulation and Big Data Assimilation"//</span>
** [[Project information at CREST site|http://www.jst.go.jp/kisoken/crest/en/project/44/14531840.html]].
** [[Koshimura Group information at Research Institute site|http://www.regid.irides.tohoku.ac.jp/index-en.html]]

</div>

<br>

<div class="virus" style="background-size: 100% 100%; padding: 50px;">

* <$link to="Disease Risk Prediction - Tsunoda Group"><span style="font-size: 25px; color: #f2ff69; font-weight: bold;">Disease Risk Prediction - Tsunoda Group</span></$link>
** <span style="font-size: 18px; color: orchid; font-weight: bold">Research Director:</span> <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Tatsuhiko Tsunoda"/><span style="font-size: 22px; color: lightblue; font-weight:bold;">//Tatsuhiko Tsunoda//</span></$button> (Professor,  The University of Tokyo).
** <span style="font-size: 18px; color: orchid; font-weight: bold">Complete Name:</span> <span style="font-size: 17px; font-style: italic; font-weight: bold">//"Exploring Etiologies, Sub-Classification, and Risk Prediction of Diseases Based on Big-Data Analysis of Clinical and whole Omics Data in Medicine"//</span>
** [[Project information at CREST site|http://www.jst.go.jp/kisoken/crest/en/project/44/14531766.html]].
** [[Tsunoda Group information at Research Institute site|http://mesm.bs.s.u-tokyo.ac.jp/index_Eng.html]]

</div>

<br>

<div class="drugs" style="background-size: 100% 100%; padding: 50px;">

* <$link to="Drug Discovery - Funatsu Group"><span style="font-size: 25px; color: #f2ff69; font-weight: bold;">Drug Discovery - Funatsu Group</span></$link>
** <span style="font-size: 18px; color: orchid; font-weight: bold">Research Director:</span> <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Kimito Funatsu"/><span style="font-size: 22px; color: lightblue; font-weight:bold;">//Kimito Funatsu//</span></$button> (Professor, University of Tokyo).
** <span style="font-size: 18px; color: orchid; font-weight: bold">Complete Name:</span> <span style="font-size: 17px; font-style: italic; font-weight: bold">//"Development of a Knowledge-Generating Platform Driven by Big Data in Drug Discovery through Production Processes"//</span>
** [[Project information at CREST site|http://www.jst.go.jp/kisoken/crest/en/project/44/e44_01.html]].
** [[Funatsu Group information at University site|http://funatsu.t.u-tokyo.ac.jp/en/home/]]

</div>

<br>

<div class="knowledge" style="background-size: 100% 100%; padding: 50px;">

* <$link to="Knowledge Discovery - Matsumoto Group"><span style="font-size: 25px; color: #f2ff69; font-weight: bold;">Knowledge Discovery - Matsumoto Group</span></$link>
** <span style="font-size: 18px; color: orchid; font-weight: bold">Research Director:</span> <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Yuji Matsumoto"/><span style="font-size: 22px; color: lightblue; font-weight:bold;">//Yuji Matsumoto//</span></$button> (Professor, Graduate School of Information Science, Nara Institute of Science and Technology).
** <span style="font-size: 18px; color: orchid; font-weight: bold">Complete Name:</span> <span style="font-size: 17px; font-style: italic; font-weight: bold">//"Knowledge Discovery through Structural Document Understanding"//</span>
** [[Project information at CREST site|http://www.jst.go.jp/kisoken/crest/en/project/44/15656596.html]].
** [[Matsumoto Group information at Research Institute site|https://cl.naist.jp/]] (Japanese Only)

</div>

<br>

<div class="pandemic" style="background-size: 100% 100%; padding: 50px;">

* <$link to="Pandemic Forecasting - Nishiura Group"><span style="font-size: 25px; color: #f2ff69; font-weight: bold;">Pandemic Forecasting - Nishiura Group</span></$link>
** <span style="font-size: 18px; color: orchid; font-weight: bold">Research Director:</span> <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Hiroshi Nishiura"/><span style="font-size: 22px; color: lightblue; font-weight:bold;">//Hiroshi Nishiura//</span></$button> (Professor, Graduate School of Medicine, Kyoto University).
** <span style="font-size: 18px; color: orchid; font-weight: bold">Complete Name:</span> <span style="font-size: 17px; font-style: italic; font-weight: bold">//"Detecting Premonitory Signs and Real-Time Forecasting of Pandemic Using Big Biological Data"//</span>
** [[Project information at CREST site|http://www.jst.go.jp/kisoken/crest/en/project/44/14532030.html]].
<br>
<div style="text-align:right">
Credit of the image: NIAID <a href="https://creativecommons.org/licenses/by/2.0">CC BY 2.0</a>
</div>
</div>

<br>

<div class="storm" style="background-size: 100% 100%; padding: 50px;">

* <$link to="Weather Prediction - Miyoshi Group"><span style="font-size: 25px; color: #f2ff69; font-weight: bold;">Weather Prediction - Miyoshi Group</span></$link>
** <span style="font-size: 18px; color: orchid; font-weight: bold">Research Director:</span> <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Takemasa Miyoshi"/><span style="font-size: 22px; color: lightblue; font-weight:bold;">//Takemasa Miyoshi//</span></$button> (Team Leader, RIKEN).
** <span style="font-size: 18px; color: orchid; font-weight: bold">Complete Name:</span> <span style="font-size: 17px; font-style: italic; font-weight: bold">//"Innovating 'Big Data Assimilation' Technology for Revolutionizing Very-Short-Range Severe Weather Prediction"//</span>
** [[Project information at CREST site|http://www.jst.go.jp/kisoken/crest/en/project/44/e44_02.html]].
** [[Miyoshi Group information at RIKEN Center for Computational Science|http://www.riken.jp/en/research/labs/r-ccs/data_assim/]]

</div>