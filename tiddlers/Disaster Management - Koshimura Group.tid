created: 20190204074522619
creator: truemrwalker
list: [[Disaster Management Better Explained]] [[Tools & Applications of Koshimura group]] [[Koshimura Group Documentation]] [[Disaster Management Elsewhere]]
modified: 20210225071417554
modifier: truemrwalker
tags: [[CREST, Big Data Application Technologies]] [[Research Groups]]
title: Disaster Management - Koshimura Group
type: text/vnd.tiddlywiki

<span style="font-size: 34px">''Establishing the most advanced disaster reduction management system by fusion of real-time disaster simulation and big data assimilation''</span>

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=470 [earthtsunami.jpg]]<div style="position: absolute; bottom: 5px; left: 10px; color: white; font-size: 9px;">Photo by Mohri UN-CECAR from flickr</div></div>

'Koshimura Group' is named such due to its leading researcher, Professor of Disaster Science <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Shunichi Koshimura"/><span style="font-size: 20px; color: #fdfea9; font-weight:bold;">//Shunichi Koshimura//</span></$button> of International Research Institute of Disaster Science, Tohoku University, where the group research is being done.

These are Professor Koshimura's own words describing the research project:

!!!!! //"Bringing together state-of-the-art disaster science, high-performance computing, information and mathematical sciences, our team will tackle the significant research challenges in disaster response and management issues which remain, particularly in the areas of “Integration of many data sources”, “Interpretation/Veracity of data content”, “Real-time big data assimilation and Mapping”. This research project aims to establish the world’s most advanced fusion of high-performance real-time disaster simulation and big data driven disaster management system to enhance ability of societies and social systems to respond promptly, sensibly and effectively to natural disasters, withstanding adversities and exploiting lessons in the future catastrophic disaster and crisis management."//
<br style="clear: left; display:block;">[img height=20px [empty.png]]

!!! ''Goal of the Project''

Natural disaster is consequence of the dynamic activity of the planetary earth, which is driven by energies from the sun and the Earth's interior. Therefore understanding the generation mechanism of itself is important to estimate short and middle-to-long term links and hence to mitigate damages. We deal with wide-range of global natural hazards, such as great earthquake and resulting tsunami, volcanic eruption, climate change, space hazard, and so on.

The goal of the project is to use big data collection in combination of supercomputer simulations in order to fully understand the powers in play and better be able to forecast natural disasters, and though not preventing them, at least minimize the damage they cause to life and structures.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;"><figure>[img width=550 [bigdatamix.jpg]]<figcaption style="font-size: 14px;  text-align: center;">//Post event damage assessment using various remote sensing.//</figcaption></figure></div>

!!! ''Big Data Collection''

<div style="position: relative; top: 0px;">

Data is collected from several sources and integrated platforms. Initially from the Japanese SIP4D system (Shared Information Platform for Disaster Management) that collects and share data from all over Japan regarding all forms of disasters in real-time and beyond in combination with e.g. the Geospatial Information Center, using a whole range of sensors on the ground, including cameras and more, and even satellites orbiting the earth that assess the damage caused by photographing the surface. 

But also independent sources like various Universities are involved, both in collecting data, eg. mobile phone data from ordinary citizens but also simulated supercomputer generated data that tries to replicate the past, and by that find crucial parameters that can be used to prevent, forecast or dampen future disasters.

The project also collaborate and exchange data with international sources such as German Aerospace Center, University of Washington (USA), NASA, NOAA (National Oceanic and atmospheric Administration. US), just to mention a few.

</div>

<br style="clear: left; display:block;">[img height=20px [empty.png]]

<div style="position: relative;  z-index: 10; float: right; margin-left: 20px;"><figure>[img width=500 [tsunamidamageforecast.png]]<figcaption style="font-size: 14px;  text-align: center;">//Computer generated Tsunami Damage Simulation Map.//</figcaption></figure></div>

!!! ''Simulating Disasters and Post process''

<div style="position: relative; top: 0px;">

Simulations plays a big importance, not just in understanding the process to the fullest, but also in real-time as a disaster is on its way or has just begun. Then advanced and fast supercomputers that forecast several possible scenarios and allocate needed resources or support in the right direction within in the right time, or help people make rapid decisions on paths to flee and places to avoid.

</div>

<br style="clear: right; display:block;">[img height=20px [empty.png]]

!! ''Moving Forward and Learning More''

Below are different areas for you to learn more about, both Disaster Management and what it contains, but also to further dig deep into the results of this project, including interactive analyzing applications that have been developed, using real data produced by the project. Additionally there are links and recommendations to study further, beyond just this project and see what other researchers around the world is doing within the same realm of knowledge. 

* [[What is it and what does it mean?|Disaster Management Better Explained]]
** A section with resources trying to give more background and understanding of Disaster Management and why this research is of importance.
* [[Hands-on-Applications and Project Data|Tools & Applications of Koshimura group]]
** A section that allows you to take a closer look at the project data and interactively analyzing some of it's results.
* [[Hardcore Understanding|Koshimura Group Documentation]]
** A section that allows you to read some of the actual research papers and similar to take it all in at the same level the project researchers are.
* [[Beyond this project|Disaster Management Elsewhere]]
** A section that offers a few paths towards other, mainly unrelated, researchers within the same fields doing similar things, to compare and move beyond.
