created: 20201113043113434
creator: truemrwalker
draft.of: User Instructions for the Cloud Reflectivity Hands-on Application
draft.title: User Instructions for the Cloud Reflectivity Hands-on Application
modified: 20210216020349785
modifier: fujihara
tags: [[User Instructions]] [[Cloud Reflectivity 3D Hands-on Application]]
title: Draft of 'User Instructions for the Cloud Reflectivity Hands-on Application'
type: text/vnd.tiddlywiki

<div style="float: left; margin-right: 20px;"><$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="cloudreflection-screenshot.png"/>[img width=400 [cloudreflection-screenshot.png]]</$button></div>

!!Application Summary
This application displays a 3D map of Kobe city in Japan and the amount of clouds above it.
<br><br>
Below are specific user instructions for this particular application. For more general basic usage instructions please visit [[User Instructions (General)]]
<br style="clear: left; display:block;">[img height=20px [empty.png]]

This application lets the user drop air reflectivity 3D data in the data module and then drag that data to the 3D module for visualization of cloud distribution, either realistically or by using scientific color ranges for a more analyzing approach. 

This application does not come with pre-loaded data, but requires the user to download and use the data provided below. (Or use ones own data if such exist, but then be aware that depending on the location origin of that data, change of the map for the 3D module might be required.)

<div style="float: left; margin-right: 20px;"><$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="cloudFiles.png"/>[img width=214 [cloudFiles.png]]</$button></div>

The data package file is a zip-file containing six files in total, where one is a small controller file, named dBZ.ctl, in text format, which is required to be loaded together with any of the others used. The other five files are five different time points and the reflection data at that specific point in time. Those files are binary and only one is loaded each time. The zip-file must be unpacked before use.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

<div style="float: left; margin-right: 20px;"><$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="cloudDataDrop.png"/>[img width=380 [cloudDataDrop.png]]</$button></div>

The user then select the dBZ file and ONE of the five other files, which are ordered and named based on chronology, to be dragged and dropped onto the designated file-drop area of the data module. 
<br style="clear: left; display:block;">[img height=20px [empty.png]]

<div style="float: left; margin-right: 20px;"><$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="cloudData.png"/>[img width=320 [cloudData.png]]</$button></div>

<div style="float: right; margin-left: 20px;"><$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="cloudDataItemDrop.png"/>[img width=500 [cloudDataItemDrop.png]]</$button></div>

When the files have been dropped correctly the data is then loaded and displayed in the data module as seen in the figure to the left. 

The user only need to be concerned by the top row though, called "reflectivity (3DArray, 1 data items)" which has to be dragged and dropped onto the 3D-data designated area on the 3D module when hoovering above it with the data in question as seen on the figure to the right.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

The cloud is then loaded and displayed as seen in the image at the top of this user instruction, and the user can begin investigating it in details. Depending on which file used, the cloud might be bigger or smaller due to the time stamp. You can move around the 3D environment the default way used by most 3D applications, like panning, zooming and selecting etc. But you can also open the properties of the 3D module and change the color scheme, and other values in order to view the cloud in various ways, like seen below.

[img width=500 [cloudInColors.png]]

!! Data

Sample data in order to use the application can be downloaded from the [[Data for the Cloud Reflectivity Hands-on Application]] page.
<br style="clear: left; display:block;">[img height=20px [empty.png]]