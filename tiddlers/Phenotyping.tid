created: 20201203044427654
creator: truemrwalker
modified: 20210215052616070
modifier: truemrwalker
tags: [[Agriculture Data Better Explained]]
title: Phenotyping
type: text/vnd.tiddlywiki

! Phenotyping

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=400 [phenotypingSA.jpg]]</div>

<div style="position: relative; top: -15px;">

Phenotype (from Greek pheno- 'showing', and type 'type') is the term used in genetics for the composite observable characteristics or traits of an organism. The term covers the organism's morphology or physical form and structure, its developmental processes, its biochemical and physiological properties, its behavior, and the products of behavior.

An organism's phenotype results from two basic factors: the expression of an organism's genetic code, or its genotype, and the influence of environmental factors. Both factors may interact, further affecting phenotype. When two or more clearly different phenotypes exist in the same population of a species, the species is called polymorphic. A well-documented example of polymorphism is Labrador Retriever coloring; while the coat color depends on many genes, it is clearly seen in the environment as yellow, black, and brown. 

Richard Dawkins in 1978 and then again in his 1982 book The Extended Phenotype suggested that one can regard bird nests and other built structures such as caddis-fly larvae cases and beaver dams as "extended phenotypes". Wilhelm Johannsen proposed the genotype-phenotype distinction in 1911 to make clear the difference between an organism's heredity and what that heredity produces. 

DNA phenotyping (fee-no-type-ing) is the process of predicting an organism's phenotype using only genetic information collected from genotyping or DNA sequencing. Significant genetic variants associated with a particular trait are discovered using a genome-wide association study (GWAS) approach, in which hundreds of thousands or millions of single-nucleotide polymorphisms (SNPs) are tested for their association with each trait of interest. Predictive modeling is then used to build a mathematical model for making trait predictions about new subjects. <span style="font-size: 12px;">[[[Above text was extracted from Wikipedia|https://en.wikipedia.org/wiki/Phenotype]][[ (1)|https://en.wikipedia.org/wiki/Phenotype]][[ (2)]|https://en.wikipedia.org/wiki/DNA_phenotyping]]</span>

</div>