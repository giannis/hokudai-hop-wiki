created: 20201104000900098
creator: truemrwalker
modified: 20201104034232704
modifier: truemrwalker
tags: [[User Instructions]]
title: User Instructions (General)
type: text/vnd.tiddlywiki

!!Basic General Instructions
Most Applications in the Hands-on-Portal uses more or less the same visualization modules, so here you will find some basic default instructions that kind of works for all or part of all applications on the site. 
<br>Click Images for a closer look at the details of the image.
<br><br>
For more detailed instruction of a specific component please visit [[User Instructions (Component Specifics)]]
<br style="clear: left; display:block;">[img height=20px [empty.png]]

@@border-top:2px solid #ff9;
---
@@

!! Expanding the size of the application

<div style="float: left; margin-right: 20px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-02-Expanded01.jpg"/>[img width=400 [HaloMergeUI-02-Expanded01.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-02-Expanded02.jpg"/>[img width=400 [HaloMergeUI-02-Expanded02.jpg]]</$button>
</div>

You can make more room for the application itself by hiding the navigation menu in the sidebar. Clicking on the ">>" symbol in the top right corner hides the side bar. Clicking on the "<<" symbol brings the sidebar back again.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

@@border-top:2px solid #ff9;
---
@@

!! Data

Many applications loads with some sample data for you to explore. If you want to load another data set, just drag and drop a file from your file system onto the "Drop Data File Here" area. You can also click the "Choose Files" button to browse the file system to the file with the data you want to use. Many applications offer additional data to be downloaded and used beyond the initial default-data
<br>
If you want to use your own data, any file that has either TAB separated, comma separated, or space separated rows with data should work. There is also support for a range of other data formats, also binary ones. Try yourself or read the description for the data reader module. 
<br>
If you have data in a format that the data component does not parse correctly, please [[send us an e-mail|mailto:wblwrld@outlook.jp]] and we can try to update the data component to understand your format too.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

@@border-top:2px solid #ff9;
---
@@

!! Selecting subsets of data

<div style="float: left; margin-right: 20px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-03-Selection01.jpg"/>[img width=400 [HaloMergeUI-03-Selection01.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-03-Selection02.jpg"/>[img width=400 [HaloMergeUI-03-Selection02.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-03-Selection03.jpg"/>[img width=400 [HaloMergeUI-03-Selection03.jpg]]</$button>
</div>

You can select subsets of data by clicking and dragging on the visualizations. If you hold down the Ctrl key (or the Mac "command" key if you are using a Mac computer) while clicking and dragging, you can select multiple data sets. Different sets will be shown in different colors, and unselected data will be shown in grey. There is a "Select All" button to select all data on all components, located on the Digital Dashboard component in the top left corner.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

@@border-top:2px solid #ff9;
---
@@

!! Choosing what data to show where

<div style="float: left; margin-right: 20px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-04-Drag.jpg"/>[img width=400 [HaloMergeUI-04-Drag.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-05-Drop01.jpg"/>[img width=400 [HaloMergeUI-05-Drop01.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-05-Drop02.jpg"/>[img width=400 [HaloMergeUI-05-Drop02.jpg]]</$button>
</div>

The data loading component shows a list of all data fields it has found in the file. You can drag fields from the data component and drop them on the visualizations to change what data is visualized. 
<br>
In this example, dragging the "Halo ID" field and dropping it on the "New Coordinate" area of the parallel coordinate component adds one more coordinate that shows this field.

<br style="clear: left; display:block;">

<div style="float: left; margin-right: 20px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-05-Drop03.jpg"/>[img width=400 [HaloMergeUI-05-Drop03.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-05-Drop04.jpg"/>[img width=400 [HaloMergeUI-05-Drop04.jpg]]</$button>
</div>

In this example, dragging the "Time" field and dropping it on the "Xaxis" area of the scatter plot component changes the scatter plot.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

@@border-top:2px solid #ff9;
---
@@

!! Zooming and panning

<div style="float: left; margin-right: 20px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-06-Zoom01.jpg"/>[img width=400 [HaloMergeUI-06-Zoom01.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-06-Zoom02.jpg"/>[img width=400 [HaloMergeUI-06-Zoom02.jpg]]</$button>
</div>

Some visualization components support zooming. You can zoom in by pressing the "+" key while the cursor is over the component you want to zoom. Pressing the "-" key zooms out again. When zoomed, you can pan the viewport with the arrow keys (i.e. the right arrow moves the zoomed viewport to the right).
<br style="clear: left; display:block;">[img height=20px [empty.png]]

@@border-top:2px solid #ff9;
---
@@

!! Changing visualization parameters

<div style="float: left; margin-right: 20px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-07-Properties01.jpg"/>[img width=400 [HaloMergeUI-07-Properties01.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-07-Properties02.jpg"/>[img width=400 [HaloMergeUI-07-Properties02.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-07-Properties03.jpg"/>[img width=400 [HaloMergeUI-07-Properties03.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-07-Properties04.jpg"/>[img width=400 [HaloMergeUI-07-Properties04.jpg]]</$button>
</div>

The visualization components have many properties you can change. Right-click on a component to access the menu, and choose "Properties". The properties window lists many properties, some that are common to most components, some that are very specific to the selected component.
<br>
In the example, changing the ~MaxDotSize property of the Halo Plot changes the size of the halos in the plot.
<br>
Some properties that can be useful include the size of dots in plots, the widths of lines, the transparency of items visualized on top of each other, and the font size to use.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

@@border-top:2px solid #ff9;
---
@@

!! Connecting data fields with visualization fields using the menu

<div style="float: left; margin-right: 20px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-08-Connect01.jpg"/>[img width=400 [HaloMergeUI-08-Connect01.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-08-Connect02.jpg"/>[img width=400 [HaloMergeUI-08-Connect02.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-08-Connect03.jpg"/>[img width=400 [HaloMergeUI-08-Connect03.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-08-Connect04.jpg"/>[img width=400 [HaloMergeUI-08-Connect04.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-08-Connect05.jpg"/>[img width=400 [HaloMergeUI-08-Connect05.jpg]]</$button>
</div>

Dragging and dropping data fields from the data component onto the visualization components is one way to set up what data should be visualized in what component. Another way is to use the menu of the Digital Dashboard component.
<br>
Right-click on the Digital Dashboard component and choose "Visualizations -> Data". This shows a list of visualization components, and you can add or remove data fields to visualize. Some more intricate ways to visualize data are more easily set up using the menu.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

In the example, selecting the Bar Chart visualization and the using the dropdown boxes to select different data to visualize changes the visualization in the Bar Chart component.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

@@border-top:2px solid #ff9;
---
@@

!! Moving parallel coordinates

<div style="float: left; margin-right: 20px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-09-MoveCoord01.jpg"/>[img width=400 [HaloMergeUI-09-MoveCoord01.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-09-MoveCoord02.jpg"/>[img width=400 [HaloMergeUI-09-MoveCoord02.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-09-MoveCoord03.jpg"/>[img width=400 [HaloMergeUI-09-MoveCoord03.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-09-MoveCoord04.jpg"/>[img width=400 [HaloMergeUI-09-MoveCoord04.jpg]]</$button>
</div>

The parallel coordinate visualization component allows you to reorder the components on the screen, which can be useful to see how different data fields correlate. Click on the coordinate title and drag the coordinate to the left or right.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

@@border-top:2px solid #ff9;
---
@@

!! Drag and drop data fields from one visualization to another

<div style="float: left; margin-right: 20px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-10-VizToVizDrag01.jpg"/>[img width=400 [HaloMergeUI-10-VizToVizDrag01.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-10-VizToVizDrag02.jpg"/>[img width=400 [HaloMergeUI-10-VizToVizDrag02.jpg]]</$button>&nbsp;
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-10-VizToVizDrag03.jpg"/>[img width=400 [HaloMergeUI-10-VizToVizDrag03.jpg]]</$button>
</div>

You can also specify what data to visualize by dragging data visualized in one component and dropping it on another visualization. Just click and drag on the data field name in the top of the visualization, and the drop the data field the same way as when dragging data fields from the data component.
<br>
In this example, the "halo radius" field is dragged from the Halo Plot component and dropped on the Coordinate #4 area of the Parallel Coordinates component. This replaces the previous data (the "x" axis position) and the parallel coordinate visualization is changed.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

@@border-top:2px solid #ff9;
---
@@

!! Selecting all data

<div style="float: left; margin-right: 20px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="HaloMergeUI-11-SelectAll.jpg"/>[img width=400 [HaloMergeUI-11-SelectAll.jpg]]</$button>
</div>

The Digital Dashboard component has a "Select All" button that selects all data again, if you quickly want to reset the selection of subsets to the initial state.
<br style="clear: left; display:block;">[img height=30px [empty.png]]