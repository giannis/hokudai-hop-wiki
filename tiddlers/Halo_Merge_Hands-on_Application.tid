created: 20160729084053262
creator: j
list: [[User Instructions for the Halo Merge Hands-on Application]]
modified: 20210215073959389
modifier: truemrwalker
tags: [[Tools & Applications of Yoshida group]] [[Hands-on Applications]] [[Webble Applications]] [[Application Overview]]
title: Halo Merge Hands-on Application
type: text/vnd.tiddlywiki

Click here to Run the Halo Merge Application. <$button style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; margin: 5px;" to="Run the Halo Merge 3D Application">Run App &#8594;</$button>

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=400 [halomergeflr.jpg]]<div style="position: absolute; bottom: 8px; left: 16px; color: gray; font-size: 9px;">Photo by NASA from flickr</div></div>

!! Summary
This application visualizes data for simulated halos in space. Halos move over time and can merge with other halos.

!! Halo Merge background information

There is a vast number of galaxies in the universe. Those galaxies vary in color, shape (spiral or elliptical) and mass size. Essentially, a single galaxy comprises of a large population of individual stars and there are galaxies with over 100 million stars. 
<br>
All of these galaxies were born at the beginning of the universe and continue to evolve until today. Stars pulled together by their own gravitational forces, are clustered in specific spots in the universe to form galaxies. 
<br>
The research is aiming to reproduce the evolution of the material and the distribution of the universe via simulations running on supercomputers. In this context, we try to identify galaxies via recognizing their halos in our simulation results. 
<br>
In cosmology, the condensed material that surrounds a population of large stars is called a halo. Specifically, in our computer simulation, the distribution of the material of the universe has been modeled as a distribution of mass particles. When this particular material is condensed into a specific area, it forms a halo. Furthermore, this whole process of forming halos essentially affects and controls the birth and nature of a galaxy. Therefore, here, in this portal-section we provide applications and tools to analyze and visualize the spatial distribution of halos and their evolution over time. 
<br>[img height=50px [empty.png]]

!! Quick User Instructions

<div style="float: left; margin-right: 40px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Halo Merge App Component"/>[img width=400 [HaloMergeScreenshot.png]]</$button>
</div>

This application visualizes data from particle simulations. The data used shows halos moving in space and merging with other halos. More in-depth user instruction details can be found in the [[User Instructions for the Halo Merge Hands-on Application]].
<br>
If you have your own data to use in the portal application, you can drag-and-drop data files onto the "Drop Data File Here" area. You can also click the "Choose Files" buttons in the application and select files this way. The application auto-loads with a small sample data set, and you can also find more data sets at the [[Data for the Halo Merge Hands-on Application]] page.
<br>
You can click and drag to select subsets of data, both in the parallel coordinate visualization and in the halo plots. Holding down the Control button while clicking and dragging creates more selections, other old selections are removed when new selections are made. You can zoom in and out in the halo plots using the + and - keys. When zoomed, you can pan around, using the arrow keys. 