created: 20191120071418207
creator: truemrwalker
list: [[Simple Neural Network Viewer Hands-on Application]]
modified: 20210205071108021
modifier: fujihara
tags: [[Application Overview]] [[Hands-on Applications]] [[Tools & Applications of Funatsu group]] [[Webble Applications]]
title: Chemical Space Analyzer Hands-on Application
type: text/vnd.tiddlywiki

Click here to Run the Chemical Space Analyzer Application. <$button style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; margin: 5px;" to="Run the Chemical Space Analyzer Hands-on Application">Run App &#8594;</$button>

!! Summary
This application allows the user to view and analyze chemical compound data in the 3D chemical space as well as 2D molecule view.

!! Chemical Space Analyzer background information
<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=400 [chemicalspace_top1.PNG]]</div>
Allows the exploration of virtual generated compound data in 3D and be able to freely select on the 3D display things that "look interesting" and study them further. A database of chemical compounds, called the Virtually Generated Compound Library, has billons of virtual compounds with hints on synthesis routes, which can theoretically be loaded and studied with this application.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

!! Quick User Instructions

<div style="float: left; margin-right: 40px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Chemical Space App Component"/>[img width=400 [Chemical Space App Screenshot.png]]</$button>
</div>

This application lets the user view chemical compound data in 3D chemical space and select any molecule and examine the details in a 2D molecule viewer. More in-depth user instruction details can be found in the [[User Instructions for the Chemical Space Hands-on Application]].
<br>
If you have your own data to use in the portal application, you can drag-and-drop data files onto the "Drop Data File Here" area. You can also click the "Choose Files" buttons in the application and select files this way. The application auto-loads with a small sample data set, and you can also find more data sets at the [[Data for the Chemical Space Analyzer Hands-on Application]] page.
<br>
You can move in the 3D space using the mouse in the default 3D environment fashion and also select any of the glowing points to be studied further in the 2D visualizer below.