created: 20161212104422111
creator: j
list: [[User Instructions for the Image Difference Hands-on Application]] [[Data for the Image Difference Hands-on Application]]
modified: 20210215074203983
modifier: truemrwalker
tags: [[Tools & Applications of Yoshida group]] [[Hands-on Applications]] [[Webble Applications]] [[Application Overview]]
title: Image Difference Hands-on Application
type: text/vnd.tiddlywiki

Click here to Run the Image Difference Application. <$button style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; margin: 5px;" to="Run the Image Difference Hands-on Application">Run App &#8594;</$button>

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=400 [https://upload.wikimedia.org/wikipedia/commons/d/d4/Keplers_supernova.jpg]]<div style="position: absolute; bottom: 8px; left: 16px; color: gray; font-size: 9px;">Photo from ~WikiMedia</div></div>

!! Summary
This is an application that creates differences between two images in different ways (the absolute value, different colors for differences in different directions, stretching the grey scale to show differences as clearly as possible, etc.).

!! Supernova background information

A supernova (/ˌsuːpərˈnoʊvə/ plural: supernovae /ˌsuːpərˈnoʊviː/ or supernovas) is a powerful and luminous stellar explosion. This transient astronomical event occurs during the last evolutionary stages of a massive star or when a white dwarf is triggered into runaway nuclear fusion. The original object, called the progenitor, either collapses to a neutron star or black hole, or is completely destroyed. The peak optical luminosity of a supernova can be comparable to that of an entire galaxy before fading over several weeks or months.
<br>
Compared to a star's entire history, the visual appearance of a supernova is very brief, perhaps spanning several months, so that the chances of observing one with the naked eye is roughly once in a lifetime. Only a tiny fraction of the 100 billion stars in a typical galaxy have the capacity to become a supernova, restricted to either those having large mass or extraordinarily rare kinds of binary stars containing white dwarfs.
<br>
Supernova searches fall into two classes: those focused on relatively nearby events and those looking farther away. Because of the expansion of the universe, the distance to a remote object with a known emission spectrum can be estimated by measuring its Doppler shift (or redshift); on average, more-distant objects recede with greater velocity than those nearby, and so have a higher redshift. Thus the search is split between high redshift and low redshift.
<br>[img height=50px [empty.png]]

!! Quick User Instructions

<div style="float: left; margin-right: 40px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Image Difference App Component"/>[img width=400 [ImageDifferenceScreenshot.png]]</$button>
</div>

This application visualizes images of the sky at different times. When comparing those images one might discover differences that might indicate supernovas. More in-depth user instruction details can be found in the [[User Instructions for the Image Difference Hands-on Application]].
<br>
If you have your own data to use in the portal application, you can drag-and-drop data files onto the "Drop Image Files Here" area. You can also click the "Choose Files" buttons in the application and select files this way. This application requires that you manually add image data to it, and a subset of those images can be found and downloaded at the [[Data for the Image Difference Hands-on Application]] page.
<br>[img height=50px [empty.png]]