created: 20191126045322018
creator: truemrwalker
list: [[User Instructions for the Tsunami Damage Visualization Hands-on Application]] [[Data for the Tsunami Damage Hands-on Application]]
modified: 20210216074144988
modifier: truemrwalker
tags: [[Application Overview]] [[Hands-on Applications]] [[Tools & Applications of Koshimura group]] [[Webble Applications]]
title: Tsunami Damage Visualization Hands-on Application
type: text/vnd.tiddlywiki

Click here to Run the Tsunami Damage Visualization Application. <$button style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; margin: 5px;" to="Run the Tsunami Damage Visualization Application">Run App &#8594;</$button>

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=400 [tsunamidamageflckr.jpg]]<div style="position: absolute; bottom: 5px; left: 10px; color: white; font-size: 9px;">Photo by U.S. Pacific Fleet from flickr</div></div>

!! Summary
This application is a visualization tool where one may examine the damage on infrastructure caused by a Tsunami simulation generated by a simulated earthquake of a certain distance and a certain magnitude.

!! Tsunami Damage background information

<div style="position: relative; top: 0px;">

Tsunami damage means loss or harm caused by a destructive tsunami. More specifically, the damage caused directly by tsunamis can be summarized into the following: 

# Deaths and injuries.
# Houses destroyed, partially destroyed, inundated, flooded, or burned.
# Other property damage and loss.
# Boats washed away, damaged or destroyed. 
# Lumber washed away.
# Marine installations destroyed.
# Damage to public utilities such as railroads, roads, bridges, power plants, water or fuel storage tanks, or wastewater facilities.

Indirect secondary tsunami damage can be: 

# Damage by fire of houses, boats, oil tanks, gas stations, and other facilities
# Environmental pollution or health hazards caused by drifting materials, oil, and hazardous waste spillages
# Outbreak of disease of epidemic proportions, which could be serious in densely populated areas.

<span style="font-size: 12px;">[[[Above text was extracted from an online article]|http://www.tsumaps-neam.eu/glossary/tsunami-damage/]]</span>

</div>

<br style="clear: left; display:block;">[img height=20px [empty.png]]

!! Quick User Instructions

<div style="float: left; margin-right: 40px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Tsunami Damage App Component"/>[img width=400 [TsunamiDamageAppScreenshot.png]]</$button>
</div>

This application lets the user view infrastructure damage data from a tsunami simulation. More in-depth user instruction details can be found in the [[User Instructions for the Tsunami Damage Visualization Hands-on Application]].
<br>
If you have your own data to use in the portal application, you can drag-and-drop data files onto the "Drop Data File Here" area. You can also click the "Choose Files" buttons in the application and select files this way. The application auto-loads with a small sample data set, and you can also find more data sets at the [[Data for the Tsunami Damage Hands-on Application]] page.
<br>
You can click and drag to select subsets of data in most visualization modules. Holding down the Control button while clicking and dragging creates more selections, other old selections are removed when new selections are made. You can zoom in and out in most modules using the + and - keys. When zoomed, you can pan around, using the arrow keys. 