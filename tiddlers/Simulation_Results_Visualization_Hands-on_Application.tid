created: 20161212101136989
creator: j
list: [[User Instructions for the Simulation Results Visualization Hands-on Application]] [[Data for the Simulation Results Visualization Hands-on Application]]
modified: 20210215074347035
modifier: truemrwalker
tags: [[Tools & Applications of Yoshida group]] [[Webble Applications]] [[Application Overview]] [[Hands-on Applications]]
title: Simulation Results Visualization Hands-on Application
type: text/vnd.tiddlywiki

Click here to Run the Simulation Results Visualization Application. <$button style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; margin: 5px;" to="Run the Simulation Results Visualization Hands-on Application">Run App &#8594;</$button>

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=400 [drkmattereu.jpg]]<div style="position: absolute; bottom: 8px; left: 16px; color: gray; font-size: 9px;">Photo by EU Space Agency from flickr</div></div>

!! Summary
This is an application that visualizes data from a universe particle simulation. These simulations are run with physical constants for which the true values are unknown set to various values, to see what values result in universes that look like our universe. The simulations run on a supercomputer and result in several terabytes of data.
<br style="clear: left; display:block;">[img height=20px [empty.png]]

!! Supercomputer generated Universe Simulation background information

How are galaxies formed? The question can feel unfathomable when the youngest galaxies are still billions of years old. 
<br>
Now, thanks to supercomputers, researchers are simulating billions of galaxies - each obeying different laws of physics for how galaxies should be formed - in order to answer the big question.
<br>
Countless theories and models have been created in the pursuit of understanding the formation of our galaxy is.
Thankfully the night sky acts as a time machine of sorts: light transported from billions of light-years away give us an insight into the past, including the early formation, of the universe.
Thanks to the information gathered, researchers have been able to use a supercomputer as a "Universe Machine." It has been programmed to generate millions of small universes so as to compare their formation to the real universe.
<br>
The galaxies are obviously rendered in very little detail compare to the real thing - these are entire galaxies we are talking about. However, they provide enough information to show us how galaxies progressively change under different applied laws of physics. 
The researchers set out to find the galaxy simulations that most resembles our own - this would mean that the law of physics is most likely to have been a strong contributing factor in real, non-simulated, life.
<br>[img height=50px [empty.png]]

!! Quick User Instructions

<div style="float: left; margin-right: 40px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Particle Simulation Result Visualization Component"/>[img width=400 [SimulationResultsVisualizationScreenshot.png]]</$button>
</div>

This is an application that visualizes data from a universe particle simulation. More in-depth user instruction details can be found in the [[User Instructions for the Simulation Results Visualization Hands-on Application]].
<br>
If you have your own data to use in the portal application, you can drag-and-drop data files onto the "Drop Data File Here" area. You can also click the "Choose Files" buttons in the application and select files this way. The application auto-loads with a small sample data set, and you can also find more data sets at the [[Data for the Simulation Results Visualization Hands-on Application]] page.
<br>
You can click and drag to select subsets of data, both in the parallel coordinate visualization and in the halo plots. Holding down the Control button while clicking and dragging creates more selections, other old selections are removed when new selections are made. You can zoom in and out in the halo plots using the + and - keys. When zoomed, you can pan around, using the arrow keys. 