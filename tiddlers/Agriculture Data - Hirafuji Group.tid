created: 20190204075503217
creator: truemrwalker
list: [[Agriculture Data Better Explained]] [[Tools & Applications of Hirafuji group]] [[Hirafuji Group Documentation]] [[Agriculture Data Elsewhere]]
modified: 20210218055038399
modifier: truemrwalker
tags: [[CREST, Big Data Application Technologies]] [[Research Groups]]
title: Agriculture Data - Hirafuji Group
type: text/vnd.tiddlywiki

<span style="font-size: 34px">''Knowledge Discovery by Constructing ~AgriBigData''</span>

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=470 [agridrone.jpg]]<div style="position: absolute; bottom: 8px; left: 16px; color: gray; font-size: 9px;">Photo by JESHOOTS.com from Pexels</div></div>

'Hirafuji Group' is named such due to its leading researcher, Professor <$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Masayuki Hirafuji"/><span style="font-size: 20px; color: #fdfea9; font-weight:bold;">//Masayuki Hirafuji//</span></$button> of the Agriculture department of Tokyo University, where the group research is mainly being done. Other members of the group are e.g. dr. Wei Guo, als of Tokyo University. Other research labs participating in this group are e.g. University of Tsukuba and research fields in Hokkaido.

These are Professor Hirafuji's own words describing the research project:

!!!!! //"Plants grow dynamically under changing environmental conditions. So time series data such as plant growth and soil moisture should be collected simultaneously for long term. Such time-series data can be analyzed as phenomena of complex systems, and the results enable objective evaluation and control in farm management and breeding. We will develop a method to construct agricultural big data (~AgriBigData) automatically using our field monitoring technologies, that is, sensor networks and drones. Also we will develop methods on the ~AgriBigData to discover new knowledge, which can be applied for optimal farming and rapid breeding."//
<br style="clear: left; display:block;">

!!! ''Goal of the Project''

The project concerns big data collection of agriculture environments, so called ~AgriBigData which in theory can accelerate advancement of smart agriculture by producing detailed knowledge of every field in real time. With smart agriculture one can improve food production system dramatically, we will be able to live longer peacefully without hard work. One of the bottle-necks of smart agriculture is data collection in real fields which the group wish to improve and advance, e.g. by the use of autonomous drone photography and sensors.

There are two types of data collected by this group. Data collected since a long time and data collected as part of the current project. The new data is photos taken by a drone flying over a field. The old data is sensor nodes placed in various places and recording many things (for example temperature) and taking photos at regular intervals. Problems the group faces regarding collection of data are for example problem with storage, access, and lack of metadata.
<br style="clear: left; display:block;">

{{Hands on protal-Hirafuji-toppage with note.pdf}}

---

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;"><figure>[img width=400 [smartagri.jpg]]<div style="position: absolute; bottom: 48px; left: 46px; color: white; font-size: 9px;">Photo by Biz4Intellia Inc from Flickr</div><figcaption style="font-size: 14px;  text-align: center;">//Smart Farming is the future of farming.//</figcaption></figure></div>

!!! ''Smart Agriculture''

<div style="position: relative; top: 0px;">

Smart Agriculture or "Smart farming" is an emerging concept that refers to managing farms using technologies like ~IoT, robotics, drones and AI to increase the quantity and quality of products while optimizing the human labor required by production.

The Internet of Things (~IoT) has provided not only a way to better measure and control growth factors, like irrigation and fertilizer, on a farm, it will change how we view agriculture in its entirely. In this article, I’ll explain what exactly a smart farm is and ~IoT will impact the future of farming.

Among the technologies available for present-day farmers are:

* Sensors: soil, water, light, humidity, temperature management
* Software:  specialized software solutions that target specific farm types or use case agnostic ~IoT platforms
* Connectivity: cellular, ~LoRa, etc.
* Location: GPS, Satellite, etc.
* Robotics: Autonomous tractors, processing facilities, etc.
* Data analytics: standalone analytics solutions, data pipelines for downstream solutions, etc.

Armed with such tools, farmers can monitor field conditions without even going to the field and make strategic decisions for the whole farm or for a single plant.

The driving force of smart farming is ~IoT —connecting smart machines and sensors integrated on farms to make farming processes data-driven and data-enabled. <span style="font-size: 12px;">[[[Above text was extracted from an online article]|https://www.iotforall.com/smart-farming-future-of-agriculture]]</span>

</div>
<br style="clear: left; display:block;">[img height=20px [empty.png]]

<div style="position: relative;  z-index: 10; float: right; margin-right: 20px;"><figure><!--<$link to="Not Licensed"><div class="stamp"><span class="tooltiptext2">This Image is Not Licensed</span></div></$link>-->[img width=450 [agriculturedrone.png]]<figcaption style="font-size: 14px;  text-align: center;">//Drones makes farming Smarter, but not without problems.//</figcaption></figure></div>

!!! ''Agricultural Drone''

<div style="position: relative; top: 0px;">

An agricultural drone is an unmanned aerial vehicle used to help optimize agriculture operations, increase crop production, and monitor crop growth. Sensors and digital imaging capabilities can give farmers a richer picture of their fields. Using an agriculture drone and gathering information from it may prove useful in improving crop yields and farm efficiency.

Agricultural drones let farmers see their fields from the sky. This bird's-eye view can reveal many issues such as irrigation problems, soil variation, and pest and fungal infestations. Multispectral images show a near-infrared view as well as a visual spectrum view. The combination shows the farmer the differences between healthy and unhealthy plants, a difference not always clearly visible to the naked eye. Thus, these views can assist in assessing crop growth and production.

Additionally, the drone can survey the crops for the farmer periodically to their liking. Weekly, daily, or even hourly, pictures can show the changes in the crops over time, thus showing possible “trouble spots”. Having identified these trouble spots, the farmer can attempt to improve crop management and production.

But the use of drones comes with many issues, such as security and safety, eg. privacy for neighbors and passer-bys, but also health issues for the plants, considering the drones downwash and other environmental angles. <span style="font-size: 12px;">[[[Above text was extracted from Wikipedia]|https://en.wikipedia.org/wiki/Agricultural_drone]]</span>

</div>
<br style="clear: right; display:block;">[img height=20px [empty.png]]

!! ''Moving Forward and Learning More''

Below are different areas for you to learn more about, both Agriculture Data and what it contains, but also to further dig deep into the results of this project, including interactive analyzing applications that have been developed, using real data produced by the project. Additionally there are links and recommendations to study further, beyond just this project and see what other researchers around the world is doing within the same realm of knowledge. 

* [[What is it and what does it mean?|Agriculture Data Better Explained]]
** A section with resources trying to give more background and understanding of Agriculture Data and why this research is of importance.
* [[Hands-on-Applications and Project Data|Tools & Applications of Hirafuji group]]
** A section that allows you to take a closer look at the project data and interactively analyzing some of it's results.
* [[Hardcore Understanding|Hirafuji Group Documentation]]
** A section that allows you to read some of the actual research papers and similar to take it all in at the same level the project researchers are.
* [[Beyond this project|Agriculture Data Elsewhere]]
** A section that offers a few paths towards other, mainly unrelated, researchers within the same fields doing similar things, to compare and move beyond.