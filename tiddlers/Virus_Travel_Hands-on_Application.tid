created: 20190419082457243
creator: truemrwalker
list: [[User Instructions for the Virus Travel Visualization Application]] [[Data for the Virus Travel Hands-on Application]]
modified: 20210216084447193
modifier: truemrwalker
tags: [[Tools & Applications of Nishiura group]] [[Application Overview]] [[Hands-on Applications]]
title: Virus Travel Hands-on Application
type: text/vnd.tiddlywiki

Click here to Run the Virus Travel Visualization Application. <$button style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; margin: 5px;" to="Run the Virus Travel Visualization Application">Run App &#8594;</$button>

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=400 [virustravel-pexels-anna-shvets.jpg]]<div style="position: absolute; bottom: 5px; left: 10px; color: gray; font-size: 9px;">Photo by Anna Shvets from Pexels</div></div>

!! Summary
This application visualizes air travel flight data (and not any actual virus related data) but it does indicates how virus most effectively transport themselves around the world.

!! Transportation and Pandemics

<div style="position: relative; top: 0px;">

With ubiquitous and fast transportation comes to a quick and extensive diffusion of a communicable disease. From an epidemiological perspective, transportation can thus be considered as a vector, particularly for passenger transportation systems. The configuration of air transportation networks shapes the diffusion of pandemics. The global air transport system is composed of airports that have different volumes and connectivity, implying that depending on the airport there is a potentially different scale and scope of diffusion. This issue concerns the early phases of a pandemic (first 10 days) where transportation systems are likely to spread any outbreak at the global level. <span style="font-size: 12px;">[[[Above text was extracted from an online article]|https://transportgeography.org/contents/applications/transportation-pandemics/]]</span>

</div>

<br style="clear: left; display:block;">

!! Quick User Instructions

<div style="float: left; margin-right: 40px;">
<$button class='tc-btn-invisible tc-tiddlylink'><$action-sendmessage $message="tm-modal" $param="Virus Travel Visualization App Component"/>[img width=400 [virusflightapp.png]]</$button>
</div>

This application shows the travel pattern of people flying across the world and by doing so spread viruses from one place to another.. More in-depth user instruction details can be found in the [[User Instructions for the Virus Travel Visualization Application]].
<br>
If you have your own data to use in the portal application, you can drag-and-drop data files onto the "Drop Data File Here" area. You can also click the "Choose Files" buttons in the application and select files this way. The application auto-loads with a small sample data set, and you can also find more data sets at the [[Data for the Virus Travel Hands-on Application]] page.
<br>
You can click and drag to select subsets of data in most visualization modules. Holding down the Control button while clicking and dragging creates more selections, other old selections are removed when new selections are made. You can zoom in and out in most modules using the + and - keys. When zoomed, you can pan around, using the arrow keys. 