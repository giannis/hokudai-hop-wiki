created: 20201106050352034
creator: truemrwalker
modified: 20210222075810858
modifier: fujihara
tags: [[Weather Prediction Better Explained]]
title: Global Weather Simulation
type: text/vnd.tiddlywiki

! Global Weather Simulation

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=500 height=300 [map-of-the-world-74045_640.jpg]]<div style="position: absolute; bottom: 8px; left: 16px; color: gray; font-size: 9px;">Image by ~WikiImages from Pixabay </div></div>

<div style="position: relative; top: 0px;">

When performing numerical weather predictions, it is important that the simulation itself be accurate, but it is also key for real-world data, based on observations, to be accurately entered into the model. Typically, weather simulations work by having the computer conduct a number of simulations based on the current state, and then entering observational data into the simulation to nudge it in a way that puts it closer to the actual state. The problem of incorporating data in the simulation — data assimilation — has become increasingly complex with the large number of types of available data, such as satellite observations and measurements taken from ground stations. Typically, supercomputers today spend an approximately equal amount of time running the simulations and incorporating the real-world data.

Now, with research that could lead to more accurate forecasts, researchers from the RIKEN Advanced Institute for Computational Science in Japan have run an enormous global weather simulation. They ran 10,240 simulations of a model of the global atmosphere divided into 112-km sectors, and then used data assimilation and statistical methods to come up with a model closely fitting the real data for a historical time period, between November 1 and November 8, 2011. The simulations were run on Japan’s flagship 10-petaflop K computer using NICAM, a simulation intended to accurately model the atmosphere.

One of the key findings is that faraway observations, several thousand kilometers in distance, may have an impact on the eventual state of the weather forecast. Data from the Great Lakes region in the United States, for instance, can have an impact on the eventual state in Europe. This finding suggests the need for further research on advanced methods that can make better use of faraway observations, as this could potentially lead to an improvement of weather forecasts. <span style="font-size: 12px;">[[[Above text was extracted from an online article]|https://www.sciencedaily.com/releases/2015/11/151110082559.htm]]</span>

According to Takemasa Miyoshi, who led the research team, “Forecasting is becoming better thanks to more powerful computers and better observational data from satellites and radars. We attempted to use a large number of samples using a relatively coarse simulation, and found that it performed quite well, fitting the actual data from the time period we chose. We are planning to use the power of the K computer’s successor, as it develops, to create tools that could be used for better weather forecasting.”

<br style="clear: left; display:block;">

!! Weather Simulations - close up view

<div style="position: relative;  z-index: 10; float: left; margin-right: 20px;">[img width=500 height=300 [superstorm.jpg]]<div style="position: absolute; bottom: 8px; left: 16px; color: gray; font-size: 9px;">Photo by NASA from flickr</div></div>

!!! Weather simulation - how does it work?

Meteorology was one of the first disciplines to harness the power of computers, but the idea of using equations to predict the weather predates the computer era. It was first proposed in 1922 by the English mathematician Lewis Fry Richardson.

Not having any computing power at his disposal, he estimated that making a useful, timely forecast would require 64,000 people to perform the calculations. Not very feasible at the time, but his theory formed the basis for weather forecasting.

How are numerical models used to predict weather and climate?

!!! Numerical Weather Prediction

The forecast starts with a creation of a three-dimensional grid consisting of many data points representing the current atmospheric conditions over a region of interest, extending from the surface to the upper atmosphere. Each of these data points contains a set of atmospheric variables, e.g. temperature, pressure, wind speed and direction, humidity and so on, coming from the observational data. The interaction and behaviour of these atmospheric variables is captured by a set of equations.

These equations can be divided into two categories - dynamical and physical. The dynamical equations treat the Earth as a rotating sphere and the atmosphere as a fluid, so describing the evolution of the atmospheric flow means solving the equations of motion for a fluid on a rotating sphere. However, this is not enough to capture the complex behaviour of the atmosphere so a number of physical equations are added to represent other atmospheric processes, such as warming, cooling, drying and moistening of the atmosphere, cloud formation, precipitation and so on.

Now, you already know that computers work in steps, so to predict a new weather state some time into the future, these equations need to be solved a number of times. The number of time steps and their length depends on a forecast timescale and type - short, medium or long term.

!!! The Butterfly Effect
Moreover, the atmosphere is a chaotic system, which means it is very susceptible to variations in the initial conditions. A tiny difference in the initial state of the atmosphere at the beginning of the simulation may lead to very different weather forecasts several days later. This concept of small causes having large effects is referred to as the butterfly effect.

You may be familiar both with the term and the associated metaphor (a butterfly flapping its wings influencing a distant hurricane several weeks later). After all, it has been used not only in science but also in popular culture. The term was actually coined by Edward Lorenz, one of the pioneers of chaos theory, who encountered the effect while studying weather modelling. In 1961 he showed that running a weather simulation, stopping it and then restarting it, produced a different weather forecast than a simulation run without stopping!

This behaviour can be explained by the way computers work - stopping of the simulation meant that the values of all variables had to be stored in a machine’s memory. The problem was that the level of precision of those stored numbers was less than the precision the computer used to compute them - the numbers were being rounded. Assuming that such small differences could have no significant effect, Lorenz rounded the numbers accurate to six decimal places (e.g. 6.174122) to three decimal places (e.g. 6.174) before printing. The simulation was restarted with those slightly different numbers and the initially small differences were amplified into different weather forecasts!

Typically, to lessen the uncertainty in weather predictions, ensemble forecasting is used. In simple terms, a number of simulations are run with slightly different initial conditions and the results are combined into probabilistic forecasts, showing how likely particular weather conditions are. If results of the ensemble runs are similar, then the uncertainty is small, and if they are significantly different then the uncertainty is bigger. <span style="font-size: 12px;">[[[Above text was extracted from an online article]|https://www.futurelearn.com/info/courses/supercomputing/0/steps/24057]]</span>

</div>

<br style="clear: right; display:block;">[img height=15px [empty.png]]