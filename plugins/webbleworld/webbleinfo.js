/*\
title: $:/plugins/webbleworld/webbleinfo.js
type: application/javascript
module-type: widget
\*/
(function() {

	/*jslint node: true, browser: true */
	/*global $tw: false */
	"use strict";

	exports.name = "webbleworld";
	exports.platforms = ["browser"];
	exports.synchronous = false;

//**********************************************************************

	var Widget = require("$:/core/modules/widgets/widget.js").widget;

	var rootAddress = "https://192.168.12.186:7443";
	//var rootAddress = "https://localhost:7443";

	var WebbleDataWidget = function(parseTreeNode,options) {
		this.initialise(parseTreeNode,options);
	};

	var WebbleImgWidget = function(parseTreeNode,options) {
		this.initialise(parseTreeNode,options);
	};

	var WebbleListWidget = function(parseTreeNode,options) {
		this.initialise(parseTreeNode,options);
	};

	var WebbleExtraImagesWidget = function(parseTreeNode,options) {
		this.initialise(parseTreeNode,options);
	};

//**********************************************************************

	/*
    Inherit from the base widget class
    */
	WebbleDataWidget.prototype = new Widget();
	WebbleImgWidget.prototype = new Widget();
	WebbleListWidget.prototype = new Widget();
	WebbleExtraImagesWidget.prototype = new Widget();

//**********************************************************************

	/*
    Render widgets to the DOM
    */
	WebbleDataWidget.prototype.render = function(parent,nextSibling) {

		// Compute our attributes
		this.computeAttributes();

		// Execute our logic
		this.execute();

		var dataView = this.createDataView(parent,nextSibling);

		// Insert the chart into the DOM and render any children
		parent.insertBefore(dataView,nextSibling);
		this.domNodes.push(dataView);
	};

	WebbleImgWidget.prototype.render = function(parent,nextSibling) {

		// Compute our attributes
		this.computeAttributes();

		// Execute our logic
		this.execute();

		var info = this.createInfo(parent,nextSibling);

		// Insert the chart into the DOM and render any children
		parent.insertBefore(info,nextSibling);
		this.domNodes.push(info);
	};

	WebbleListWidget.prototype.render = function(parent,nextSibling) {

		// Compute our attributes
		this.computeAttributes();

		// Execute our logic
		this.execute();

		var list = this.createList(parent,nextSibling);

		// Insert the chart into the DOM and render any children
		parent.insertBefore(list,nextSibling);
		this.domNodes.push(list);
	};

	WebbleExtraImagesWidget.prototype.render = function(parent,nextSibling) {

		// Compute our attributes
		this.computeAttributes();

		// Execute our logic
		this.execute();

		var imView = this.createExtraImagesView(parent,nextSibling);

		// Insert the chart into the DOM and render any children
		parent.insertBefore(imView,nextSibling);
		this.domNodes.push(imView);
	};


//**********************************************************************

	WebbleDataWidget.prototype.createDataView = function(parent,nextSibling) {
		var self = this;

		var ul = document.createElement("ul");

		$tw.utils.httpRequest({
			url: rootAddress + "/api/webbles/"+this.webble,
			callback: function (error,data){

				if (error) {
					console.log("ERROR:"+error);
				}
				else {
					var w = JSON.parse(data);

					//console.log(w.files);

					var files = !self.ext ? w.files :
						w.files.filter(function(f) { return f.endsWith(self.ext); });

					files.forEach(function(f) {

						var li = document.createElement("li");
						var a = document.createElement('a');

						a.appendChild(document.createTextNode(f));
						a.title = f;

						// download attribute doesn't seem to work -- maybe in the future
						//a.download = f;

						a.target = "_blank"; // To enable download of the href'd file
						a.href = rootAddress + "/files/webbles/" + w.webble.templateid + "/"
							+ w.webble.templaterevision + "/" + f;

						li.appendChild(a);
						ul.appendChild(li);
					});
				}
			}});
		return ul;
	};

	WebbleImgWidget.prototype.createInfo = function(parent,nextSibling) {
		var self = this;

		var img = document.createElement("img");

		$tw.utils.httpRequest({
			url: rootAddress + "/api/webbles/"+this.webble,
			callback: function (error,data){

				if (error) {
					console.log("ERROR:"+error);
				}
				else {
					// $tw.wiki.addTiddler({title:tiddlername, text:data, type: "text/x-markdown"});
					console.log(data);
					//img.src = "http://www.html5canvastutorials.com/demos/assets/darth-vader.jpg";

					var w = JSON.parse(data);

					img.src = w.webble.image.startsWith("data:") ? w.webble.image : // In case it's a dataURI
						rootAddress + "/" + w.webble.image;
				}
			}});
		return img;
	};

	WebbleListWidget.prototype.createList = function(parent,nextSibling) {
		var self = this;

		var ul = document.createElement("ul");

		$tw.utils.httpRequest({
			url: rootAddress + "/api/webbles?q="+this.query,
			callback: function (error,data){

				if (error) {
					console.log("ERROR:"+error);
				}
				else {
					// $tw.wiki.addTiddler({title:tiddlername, text:data, type: "text/x-markdown"});
					//console.log(data);

					var webbles = JSON.parse(data);

					//console.log(webbles);
					webbles.forEach(function(w) {
						var li = document.createElement("li");
						var textNode = document.createTextNode(w.webble.displayname);
						li.appendChild(textNode);
						ul.appendChild(li);
					});
				}
			}});
		return ul;
	};

	WebbleExtraImagesWidget.prototype.createExtraImagesView = function(parent,nextSibling) {
		var self = this;


		var ul = document.createElement("p");

		$tw.utils.httpRequest({
			url: rootAddress + "/api/webbles/"+this.webble,
			callback: function (error,data){

				if (error) {
					console.log("ERROR:"+error);
				}
				else {
					var w = JSON.parse(data);

					//console.log(w.files);

					var files = !self.im ? w.files :
						w.files.filter(function(f) { return f.endsWith(self.im); });

					files.forEach(function(f) {

						// var li = document.createElement("li");
						var a = document.createElement("a");
						var img = document.createElement('img');
						img.src = rootAddress + "/files/webbles/" + w.webble.templateid + "/"
							+ w.webble.templaterevision + "/" + f;

						if(self.imw != "") {
							img.width = self.imw;
						}
						a.appendChild(img);
						a.href = img.src;
						a.target = "new";
						ul.appendChild(a);
						// li.appendChild(a);
						// ul.appendChild(li);
					});
				}
			}});
		return ul;

		// var img = document.createElement("img");;

		// $tw.utils.httpRequest({
		// 	url: rootAddress + "/api/webbles/"+this.webble,
		// 	callback: function (error,data){

		// 		if (error) {
		// 			console.log("ERROR:"+error);
		// 		}
		// 		else {
		// 			var w = JSON.parse(data);

		// 		    console.log(w.files);

		// 			var files = !self.im ? w.files :
		// 				w.files.filter(function(f) { return f.endsWith(self.im); });

		// 			files.forEach(function(f) {

		// 			    img.src = rootAddress + "/files/webbles/" + w.webble.templateid + "/"
		// 					+ w.webble.templaterevision + "/" + f;

		// 			    if(self.imw != "") {
		// 			    	img.width = self.imw;
		// 			    }
		// 			    return; // if more than one file matches, just use the first one
		// 			});
		// 		}
		// }});
		// return img;
	};

//**********************************************************************

	/*
    Compute the internal state of the widget
    */
	WebbleDataWidget.prototype.execute = function() {

		// Get the parameters from the attributes
		this.webble = this.getAttribute("webble", "fundamental");
		this.ext = this.getAttribute("ext", "");
	};

	WebbleImgWidget.prototype.execute = function() {

		// Get the parameters from the attributes
		this.webble = this.getAttribute("webble", "fundamental");
	};

	WebbleListWidget.prototype.execute = function() {

		// Get the parameters from the attributes
		this.query = this.getAttribute("query", "basic");
	};

	WebbleExtraImagesWidget.prototype.execute = function() {

		// Get the parameters from the attributes
		this.webble = this.getAttribute("webble", "fundamental");
		this.im = this.getAttribute("im", ".jpg");
		this.imw = this.getAttribute("imw", "");
	};

//**********************************************************************

	exports.webbledata = WebbleDataWidget;
	exports.webbleimage = WebbleImgWidget;
	exports.webblelist = WebbleListWidget;
	exports.webbleims = WebbleExtraImagesWidget;

})();
