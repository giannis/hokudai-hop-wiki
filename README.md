# Overview

This is the repository of the Hands-on Portal TiddlyWiki wiki. This wiki is meant to
be accessed and edited via both:

* The Webble World platform
* The default TiddlyWiki server

More detailed information (and all the information contained in this file) are available at
the Webble World platform's repository at the following URL:

https://github.com/truemrwalker/wblwrld3/tree/master/app/wikis/README.md

# The Hands-on Portal TiddlyWiki

The Hands-on Portal wiki is hosted under the following gitlab repository: 
https://gitlab.com/giannis/hokudai-hop-wiki

It can be cloned with the following command:

```
git clone https://gitlab.com/giannis/hokudai-hop-wiki.git
```

The Hands-on Portal wiki is a normal TiddlyWiki that has the following characteristics:

1. Contains a ```$__config_tiddlyweb_host.tid```
2. Contains a few extra macros in the file ```$__tags_Macro.tid```
3. Contains a plugin in the file ```$__plugins_webbleworld.tid```

## Host

The file:

```
tiddlers/$__config_tiddlyweb_host.tid
```

Has the following contents:

```
title: $:/config/tiddlyweb/host

$protocol$//$host$/api/wiki/hop/
```

When the wiki is edited outside the Webble World platform, via the TiddlyWiki server, the contents 
of that file should be temporarily deleted.

## Webble application macro

The file:

```
tiddlers/$__tags_Macro.tid
```

Contains a few Webble World and Hands-on Portal specific TiddlyWiki macros.

For example the macro ```webble-app``` embeds a whole Webble World application (implemented as a Webble)
inside the target TiddlyWiki post (called tiddler).

The usage of the ```webble-app``` macro is: ```<<webble-app WEBBLE_ID>```.

For example, the following text embeds the Webble World platform that only loads the fundamental webble:

```
<<webble-app fundamental>>
```

The file ```tiddlers/$__tags_Macro.tid``` contains more macros that can be used in the Hands-on Portal
wiki.

## Webble World plugin

Apart from the macros the Hands-on Portal wiki also contains some widgets that are much more powerful than
macros since they can run arbitrarily complex javascript code.

These widgets are implemented inside a plugin called ```webbleworld```. The plugin with all its metadata is
contained under the ```plugins/webbleworld``` sub-directory.

An example usage of the widgets implemeted by the ```webbleworld``` plugin is the following:

```
<$webbledata webble="supernovaclassifier" ext=".jpg"/>
<$webbleimage webble="supernovaclassifier"/>
<$webblelist query="basic"/>
```

The javascript file ```plugins/webbleworld/webbleinfo.js``` implements those two widgets that can be used in
the Hands-on Portal wiki inside any tiddler.
